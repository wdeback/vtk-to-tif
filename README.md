Installation
------------

- Download and install miniconda

  `https://conda.io/miniconda.html`


- Clone repository via ssh or https

  `git clone git@gitlab.com:wdeback/vtk-to-tif.git`

or

  `git clone https://wdeback@gitlab.com/wdeback/vtk-to-tif.git`

- Change into directory

  `cd vtk-to-tif`

- Create an environment for the required python packages

  `conda env create -f environment.yml` (do not use `conda create --name vtktotif --file environment.yml`)

where `vtktotif` is an arbitrary name for the environment.

- Activate the environment

  `source activate vtktotif`


Usage
-----

```
$ python VTK-to-TIFF.py --help
usage: VTK-to-TIFF.py [-h] -i INPUT -o OUTPUT [-f {8,16,32,64}]

Convert VTK to TIFF stack

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        Input VTK file
  -o OUTPUT, --output OUTPUT
                        Output TIFF file
  -f {8,16,32,64}, --format {8,16,32,64}
                        Output format: 8, 16, 32, 64 bit

```

Examples
--------

- Convert VTK file to TIFF stack

  `python VTK-to-TIFF.py -i input.vtk -o output.tif`

- Specify a bit format (here: 8bit) for the TIFF output

  `python VTK-to-TIFF.py -i input.vtk -o output.tif -f 8`



