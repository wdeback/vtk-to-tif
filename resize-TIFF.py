import argparse
parser = argparse.ArgumentParser(description='Resizes (multipage) TIFF image')
parser.add_argument('-i','--input', type=str, help='input filename', required=True)
parser.add_argument('-o','--output', type=str, help='output filename', required=True)
parser.add_argument('-m','--mode', type=int, help='order of spline (0=nearest neighbor, 3 is bicubic)', required=True)
parser.add_argument('-f','--factor', type=float, help='scaling factor', required=True)
args = parser.parse_args()

from skimage.external import tifffile
from scipy.ndimage.interpolation import zoom
import numpy as np
import warnings

image = tifffile.imread(args.input)
print(image.shape)

if args.mode > 0:
    nans = np.sum(~np.isfinite(image))
    if nans > 0:
        warnings.warn('Image contains NANs. Interpolation may lead to undesirable results!')

image_small = zoom(image, zoom=args.factor, order=args.mode)
print(image_small.shape)

tifffile.imsave(args.output, image_small, compress=6)

