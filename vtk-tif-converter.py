# coding: utf-8

# In[148]:

import argparse
parser = argparse.ArgumentParser(description='Convert VTK to TIFF stack')
parser.add_argument('-i', '--input', type=str, help='Input file (vtk or tif)', required=True)
parser.add_argument('-o', '--output', type=str, help='Output file (vtk or tif)', required=True)
parser.add_argument('-f', '--format', type=int, choices=[8, 16, 32, 64], help='Output format: 8, 16, 32, 64 bit', 
required=False)
parser.add_argument('-l', '--legacy', action='store_true', help='if converting to VTK, store as ASCII legacy format', required=False)

args = parser.parse_args()


# In[149]:

import numpy as np
def load_vtk_as_numpy_array(filename):
    '''
    Reads a VTK file and returns it as an numpy array.
    Only tested with StructuredGrid
    See: https://stackoverflow.com/questions/23138112/vtk-to-maplotlib-using-numpy
    '''
    from tvtk.api import tvtk
    reader = tvtk.GenericDataObjectReader()
    reader.file_name = filename
    reader.update()
    ug = reader.output
    shape = ug.dimensions
    shape[0], shape[2] = shape[2], shape[0] # set z-size as first element
    array = ug.point_data.scalars.to_array()
    array = np.reshape(array, shape)
    print("nonzero: ", np.count_nonzero(array))
    print("min: ", np.min(array))
    print("mean: ", np.mean(array))
    print("max: ", np.max(array))
    print("type: ", array.dtype)
    if array.dtype == np.float64:
        raise ValueError('Cannot read 64bit VTK files.')
    return array

def save_as_TIFF_stack(array, filename):
    from skimage.external import tifffile
    print("Saving to file: ", filename)
    tifffile.imsave(filename, array, compress=6)


def read_TIFF_stack(filename):
    from skimage.external import tifffile
    return tifffile.imread(filename)

def save_as_VTK(array, filename, legacy=True):
    #https://pyscience.wordpress.com/2014/09/06/numpy-to-vtk-converting-your-numpy-arrays-to-vtk-arrays-and-files/
    #NumPy_data_shape = NumPy_data.shape
    #VTK_data = numpy_support.numpy_to_vtk(num_array=NumPy_data.ravel(), deep=True, array_type=vtk.VTK_FLOAT)

    #https://stackoverflow.com/questions/18005324/trying-to-save-vtk-files-with-tvtk-made-from-numpy-arrays
    import numpy as np
    from tvtk.api import tvtk, write_data

    assert( isinstance(array, np.ndarray) )
    assert( isinstance(filename, str) )
    
    # simple stack
    if array.ndim == 3:
        # move the z axis as last 
        array = np.moveaxis(array, 0, -1)
        grid = tvtk.ImageData(spacing=(1,1,1), origin=(0,0,0), dimensions=array.shape)
        grid.point_data.scalars = array.ravel(order='F')
        grid.point_data.scalars.name = 'channel'
        print('d = 3')
        
    # multiple scalar fields (multistack)
    elif array.ndim > 4: 
        # move the z axis as last 
        array = np.moveaxis(array, 1, -1)
        grid = tvtk.ImageData(spacing=(1,1,1), origin=(0,0,0), dimensions=array.shape)
        grid.point_data.scalars = array[0].ravel(order='F')
        grid.point_data.scalars.name = 'channel1'
        print('d > 3')
        for i in range(1, array.shape[0]):
            grid.point_data.add_array(array[i].ravel(order='F'))
            grid.point_data.get_array(i).name = 'channel{}'.format(i+1)
            grid.point_data.update()
    else:
        raise ValueError('Input array must be have 3 or 4 dimensions.')
            
    # Writes legacy ".vtk" format if filename ends with "vtk", otherwise
    # this will write data using the newer xml-based format.
    if args.legacy:
        if filename.endswith('.vtk'):
            write_data(grid, filename)
        else:
            write_data(grid, filename+'.vtk') # add '.vtk' to enforce legacy format
    else:
        if filename.endswith('.vtk'):
            write_data(grid, filename[:-4]) #remove .vtk to enforce XML format
        else:
            write_data(grid, filename)
            

# In[150]:

print(args)

if args.input.endswith('.vtk'):
    print(('Reading ', args.input))
    array = load_vtk_as_numpy_array(args.input)

    if args.format is not None:
        if args.format == 8:
            array = np.asarray(array, np.uint8)
        if args.format == 16:
            array = np.asarray(array, np.uint16)
        if args.format == 32:
            array = np.asarray(array, np.float32)
        if args.format == 64:
            array = np.asarray(array, np.float64)

    print('Output format = ', array.dtype)
    print("nonzero: ", np.count_nonzero(array))
    print("min: ", np.min(array))
    print("mean: ", np.mean(array))
    print("max: ", np.max(array))
    print("type: ", array.dtype)
    save_as_TIFF_stack(array, args.output)
    print(('Output written to ', args.output))

elif args.input.endswith('.tif') or args.input.endswith('.tiff'):
    print(('Reading ', args.input))
    array = read_TIFF_stack(args.input)
    save_as_VTK(array, args.output)
    print(('Output written to ', args.output))
else:
    raise ValueError('Unknown input format')
print('Done')
